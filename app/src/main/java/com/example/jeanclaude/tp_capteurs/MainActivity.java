package com.example.jeanclaude.tp_capteurs;

import android.app.Service;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

/**
 * Petit exemple hyper simple qui montre comment utiliser certains capteurs, s'y abonner et s'en
 * désabonner aux bons endroits, et récupérer les valeurs associées (dans le logcat et un peu
 * sur le layout ici)
 * Pensez aussi à regarder le step_counter, le motion sensor, etc.
 */

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Sensor accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

        Sensor light = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        manager.registerListener(this, light, SensorManager.SENSOR_DELAY_UI);

        Sensor champ_magnetique = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        manager.registerListener(this,champ_magnetique,SensorManager.SENSOR_DELAY_UI);

        Sensor proximite = manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        manager.registerListener(this,proximite,SensorManager.SENSOR_DELAY_UI);

        Sensor gyroscope = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        manager.registerListener(this,gyroscope,SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Sensor accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        manager.unregisterListener(this, accelerometer);

        Sensor light = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
        manager.unregisterListener(this, light);

        Sensor champ_magnetique = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        manager.unregisterListener(this, champ_magnetique);

        Sensor proximite = manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        manager.unregisterListener(this, proximite);

        Sensor gyroscope = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        manager.unregisterListener(this, gyroscope);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {

            case Sensor.TYPE_ACCELEROMETER:
                onAccelerometerChanged(event);

            case Sensor.TYPE_MAGNETIC_FIELD:
                onMagneticFieldChanged(event);
                break;

            case Sensor.TYPE_PROXIMITY:
                onProximityChanged(event);
                break;

            case Sensor.TYPE_LIGHT:
                onLightChanged(event);
                break;

            case Sensor.TYPE_GYROSCOPE:
                onGyroscopeChanged(event);
                break;
        }
    }

    private void onLightChanged(SensorEvent event) {
        float illuminance;
        illuminance = event.values[0];
        Log.d("TP capteurs : Light", "light " + illuminance + " lux");
        TextView light = (TextView) findViewById(R.id.txt_light);
        light.setText("" + illuminance);
    }

    private void onProximityChanged(SensorEvent event) {
        float distance;
        distance = event.values[0];
        Log.d("TP capteurs", "Proximity : " + distance + " cm");
        TextView proximity = (TextView) findViewById(R.id.txt_proximity);
        proximity.setText("" + distance);
    }

    private void onAccelerometerChanged(SensorEvent event) {
        float x, y, z;
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        Log.d("TP_capteurs", "accéléromètre x = " + x + ", y = " + y + ", z = " + z);
        TextView acce_x = (TextView) findViewById(R.id.x);
        TextView acce_y = (TextView) findViewById(R.id.y);
        TextView acce_z = (TextView) findViewById(R.id.z);
        acce_x.setText("" + x);
        acce_y.setText("" + y);
        acce_z.setText("" + z);
    }

    private void onMagneticFieldChanged(SensorEvent event) {
        float x, y, z;
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        Log.d("TP_capteurs", "champ magnétique x = " + x + ", y = " + y + ", z = " + z);
    }

    private void onGyroscopeChanged(SensorEvent event) {
        float x, y, z;
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        Log.d("TP_capteurs", "gyroscope x = " + x + ", y = " + y + ", z = " + z);
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

}